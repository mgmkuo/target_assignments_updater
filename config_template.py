"""
Config file for target_assignment_updater_files

"""
redshift_creds = {'database': 'dev',
                  'host': 'viacom-vantage3.caijrp92niv7.us-east-1.redshift.amazonaws.com',
                  'user': '',
                  'password': '',
                  'port': 5439}


s3_creds = {'access_key_id':'',
           'secret_access_key':'',
            'bucket':'mtv-fng-temp'}

db_name = 'fox'

# Assignments and ID tables writing info to
#target_assignment_table = 'maggie_target_assignments_test'
#target_id_table = 'maggie_target_test'
target_assignment_table = 'vantage_target_assignments'
target_id_table = 'vantage_targets'

"""
PARAMETERS FOR ADDING/UPDATING TARGETS
"""
# Location of pidlist
pid_list_file_path = 'home/robert/data/fox_vantage/targets/mri_targets/to_upload/targ_ea_gamer.csv'# None if table already in redshift
pid_list_table_name = None#'targ_vtech' if table already in redshift

# Target info
amrld_only = False
target_name = 'ea_gamer'
demo = 'P2+'
target_description_full ="""

"""
target_description = target_description_full


### PARAMETERS FOR BUILDING MRI/AMRLD COMBO PIDLIST: ###
### (Toggle with target_name parameter above) ###

# For uploading amrld+mri pids
base_fname = 'targ_vtech'
# Where are the MRI/AMRLD pids saved on your local machine?
amrld_dir = '/home/robert/data/fox_vantage/targets/amrld_targets/to_upload/'
mri_dir = '/home/robert/data/fox_vantage/targets/mri_targets/to_upload/'
if amrld_only:
    source_dir = amrld_dir
else:
    source_dir = mri_dir
source_file_path = '{0}/{1}.csv'.format(source_dir, base_fname)
# Where should the AMRLD/MRI combo pidlist be saved on your local machine?
pid_list_save_to_file_path = '{0}/{1}_amrld.csv'.format(source_dir, base_fname)


"""
FOR REFORMATTING PIDS CSV FROM hh_num, person_id, effective_date
TO hh_num, person_id, start_date, end_date

PLEASE PUT '/' AT END OF PATH
"""

pids_to_reformat_directory = '/home/vagrant/documents/notebooks/pids_robert_100918/'
