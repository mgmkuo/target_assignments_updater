
#__FILES__
* assign_and_upload_targets.py : For adding new target assignments or update existing ones.
* build_pidlists_from_amrld.py : For merging pids of targets from AMRLD wtih MRI.  
_Target options:_ jaguar_proxy, metro_pcs
* reformat_pids.py: For reformatting 1st party PIDs list
* update_target_sizes.py : For updating target sizes. Averages over 12 week period ending with amrld_npx_persons latest br_date
* config_template.py : config for files above. Rename as config.py  

To execute, do 'python name_of_file.py'. Change parameters through config files.


#__CONFIG FILE PARAMETERS__

##Using assign_and_uplaod_targets.py:  
Fill in AWS, S3 creds as well as
* _target_assignment_table_
* _target_id_table_
* _pid_list_file_path_
* _pid_list_table_name_
* _target_name_
* _demo_
* _target_description_

### Usage notes:
###Pidlist table names  
CSV of PIDs outputted from MriPidListBuilder:
This package is in Python3. CSV file contains pidlist information only.
Does not incorporate demo of pids (will be added later).
Columns are hh_num, person_id, start_date (of survey), end_date (of survey)
No need for "fox." prefix when entering table names.  

If output csv on local machine, use _pid_list_file_path_    
```
pid_list_file_path =  '/home/vagrant/documents/notebooks/metro_pcs_targest_from_pidlistbuilder.csv'
pid_list_table_name = None
```

If table already uploaded on redshift, fill in _pid_list_table_name_   
```
pid_list_table_name = 'targ_tmob_prox'  
pid_list_file_path =  None
```

###Target Info  
_target_name_: Find in target ID table  

_demo_: Find in excel spreadsheet with target info (or see description in target ID table)
  
_target_description_: Not needed for updating targets. Enter None to leave blank.


##Using build_pidlists_from_amrld.py:
Besides AWS and S3 creds, only need to fill in
* _target_name_
* _mri_file_path_
* _pid_list_save_to_file_path_

##Using update_target_sizes.py:
Besides AWS and S3 creds, only need to fill in target assignments and target ID assignments table names:
* _target_assignment_table_
* _target_id_table_
