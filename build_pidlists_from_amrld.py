"""
#!/usr/bin/env python2

Custom functions for building targets based on AMRLD data.
Combines AMRLD data with MRI data.
Does not include demo group info.

"""
import assign_and_upload_targets as targets
import config
import datetime
import pandas as pd


def build_jaguar_pidlist(rs_client, source_file_path, pid_list_save_to_file_path):
    """
    Combines AMLRD pids with MRI pids. Currently handles "or" statements (10/1/18).
    """
    jaguar_mri = pd.read_csv(source_file_path, index_col=0)
    jaguar_mri['start_date'] = map(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'), jaguar_mri['start_date'])
    jaguar_mri['end_date'] = map(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'), jaguar_mri['end_date'])
    cutoff_date = jaguar_mri['end_date'].unique().max()

    jaguar_amrld_sql_query = """
    SELECT DISTINCT
           hh_num,
           person_id,
           MIN(start_date) OVER (PARTITION BY hh_num, person_id) start_date,
           MAX(end_date) OVER (PARTITION BY hh_num, person_id) end_date
    FROM amrld.amrld_npx_persons
    WHERE hh_num IN (
      SELECT DISTINCT hh_num
      FROM amrld.amrld_npx_hhs
      WHERE (hh_income_detail >=11 OR hh_income_amt >=103 or hh_income >= 10)
        )
    AND start_date <= '{}' --pull date from MRI data
    ;
    """.format(cutoff_date)
    jaguar_amrld = rs_client.select_as_dataframe(jaguar_amrld_sql_query)
    jaguar_amrld['end_date'] = jaguar_amrld['end_date'].replace(datetime.date(9999, 12, 31),
                                                                datetime.datetime.now().date())
    jaguar_amrld['start_date'] = pd.to_datetime(jaguar_amrld['start_date'])
    jaguar_amrld['end_date'] = pd.to_datetime(jaguar_amrld['end_date'])
    jaguar_df = pd.concat([jaguar_mri, jaguar_amrld], axis=0)
    jaguar_df = jaguar_df.groupby(['hh_num', 'person_id']).agg({'start_date': 'min', 'end_date': 'max'}).reset_index()
    jaguar_df.to_csv(pid_list_save_to_file_path)
    print 'Jaguar proxy pidlist saved at: {}'.format(pid_list_save_to_file_path)

def build_jaguar_pidlist_main():
    rs_client, s3_client_object = targets.initialize_redshift(config.redshift_creds, config.s3_creds, config.db_name)
    build_jaguar_pidlist(rs_client, config.source_file_path, config.pid_list_save_to_file_path)

def build_metro_pcs_pidlist(rs_client, source_file_path, pid_list_save_to_file_path):
    """
    Handles "and" statement.
    """
    metro_pcs_mri = pd.read_csv(source_file_path)
    end_date = metro_pcs_mri['end_date'].unique().max()
    metro_pcs_sql_query = """
    SELECT DISTINCT
           hh_num,
           person_id,
           MIN(start_date) OVER (PARTITION BY hh_num, person_id) start_date,
           MAX(end_date) OVER (PARTITION BY hh_num, person_id) end_date
    FROM amrld.amrld_npx_persons
    WHERE hh_num IN (
    SELECT DISTINCT hh_num
    FROM amrld.amrld_npx_hhs
    WHERE hh_income BETWEEN 2 AND 8
    AND (county_size = 6 OR county_size = 7))
    AND start_date <= '{}' --pull date from MRI data
    ;
    """.format(end_date)
    metro_pcs_amrld = rs_client.select_as_dataframe(metro_pcs_sql_query)
    metro_pcs_df = pd.merge(metro_pcs_mri, metro_pcs_amrld, how='inner', on=['hh_num', 'person_id']
                        ).drop(columns=['start_date_y', 'end_date_y']).rename(
        columns={'start_date_x': 'start_date', 'end_date_x': 'end_date'})

    print 'size of AMRLD and MRI union dataframe: {}'.format(metro_pcs_df.shape)
    metro_pcs_df.to_csv(pid_list_save_to_file_path, index=False)
    print 'Metro PCS pidlist saved at : {}'.format(pid_list_save_to_file_path)

def build_metro_pcs_pidlist_main():
    rs_client, s3_client_object = targets.initialize_redshift(config.redshift_creds, config.s3_creds, config.db_name)
    build_metro_pcs_pidlist(rs_client, config.source_file_path, config.pid_list_save_to_file_path)


def build_toyota_prius_pidlist(rs_client, source_file_path, pid_list_save_to_file_path):
    """
    Handles "and" statement.
    """
    toyota_prius_mri = pd.read_csv(source_file_path)
    end_date = toyota_prius_mri['end_date'].unique().max()
    toyota_prius_sql_query = """
    SELECT DISTINCT
           hh_num,
           person_id,
           MIN(start_date) OVER (PARTITION BY hh_num, person_id) start_date,
           MAX(end_date) OVER (PARTITION BY hh_num, person_id) end_date
    FROM amrld.amrld_npx_persons
    WHERE hh_num IN (
    SELECT DISTINCT hh_num
    FROM amrld.amrld_npx_hhs
    WHERE hh_income >= 9)
    AND start_date <= '{}' --pull date from MRI data
    ;
    """.format(end_date)
    toyota_prius_amrld = rs_client.select_as_dataframe(toyota_prius_sql_query)
    toyota_prius_df = pd.merge(toyota_prius_mri, toyota_prius_amrld, how='inner', on=['hh_num', 'person_id']
                        ).drop(columns=['start_date_y', 'end_date_y']).rename(
        columns={'start_date_x': 'start_date', 'end_date_x': 'end_date'})

    print 'size of AMRLD and MRI union dataframe: {}'.format(toyota_prius_df.shape)
    toyota_prius_df.to_csv(pid_list_save_to_file_path, index=False)
    print 'Toyota Prius pidlist saved at : {}'.format(pid_list_save_to_file_path)

def build_toyota_prius_pidlist_main():
    rs_client, s3_client_object = targets.initialize_redshift(config.redshift_creds, config.s3_creds, config.db_name)
    build_toyota_prius_pidlist(rs_client, config.source_file_path, config.pid_list_save_to_file_path)

def build_vtech_pidlist(rs_client, pid_list_save_to_file_path):
    """Uses only AMRLD fields"""
    vtech_sql_query = """
    SELECT DISTINCT
           hh_num,
           person_id,
           MIN(start_date) OVER (PARTITION BY hh_num, person_id) start_date,
           MAX(end_date) OVER (PARTITION BY hh_num, person_id) end_date
    FROM amrld.amrld_npx_persons
    WHERE hh_num IN (
    SELECT DISTINCT hh_num
    FROM amrld.amrld_npx_hhs
    WHERE child_2_to_5 = TRUE
       OR child_6_to_11 = TRUE)
    AND start_date >= '2016-01-01'
    ;
    """
    #     compare to
    #     """
    #     DROP TABLE IF EXISTS tmp_vtech;
    # SELECT hh_num*100 + person_id AS pid,
    #        ROUND(AVG(CASE WHEN AGE BETWEEN 18 AND 99 AND sex = 'F'
    #                            AND (child_2_to_5 = TRUE OR child_6_to_11 = TRUE)
    #                  THEN 1 ELSE 0 END)) AS vtech INTO TEMP tmp_vtech
    # FROM (SELECT person_id,
    #              hh_num,
    #              p.start_date,
    #              p.end_date,
    #              MIN(p.start_date) OVER (PARTITION BY hh_num,person_id) AS min_start_date,
    #              AGE,
    #              sex,
    #              child_2_to_5,
    #              child_6_to_11
    #       FROM amrld.amrld_npx_persons p
    #         LEFT JOIN amrld.amrld_npx_hhs h USING (hh_num)
    #       WHERE p.start_date BETWEEN h.start_date AND h.end_date
    #       AND   p.end_date > '2016-01-01')
    # WHERE start_date = min_start_date
    # GROUP BY hh_num,
    #          person_id;
    #     """

    vtech_amrld = rs_client.select_as_dataframe(vtech_sql_query)
    vtech_df = vtech_amrld

    vtech_df.to_csv(pid_list_save_to_file_path, index=False)
    print 'VTECH pidlist saved at : {}'.format(pid_list_save_to_file_path)

def build_vtech_pidlist_main():
    rs_client, s3_client_object = targets.initialize_redshift(config.redshift_creds, config.s3_creds, config.db_name)
    build_vtech_pidlist(rs_client, config.pid_list_save_to_file_path)


def build_att_pidlist(rs_client, pid_list_save_to_file_path):
    """Uses only AMRLD fields"""
    att_sql_query = """
    SELECT DISTINCT
           hh_num,
           person_id,
           MIN(start_date) OVER (PARTITION BY hh_num, person_id) start_date,
           MAX(end_date) OVER (PARTITION BY hh_num, person_id) end_date
    FROM amrld.amrld_npx_persons
    WHERE ((language = 2 OR language = 3 OR language = 4) AND (origin = 2))
    AND start_date >= '2017-10-01' --amrld_npx_persons.origin started in Q4 2017
    ;
    """

    att_amrld = rs_client.select_as_dataframe(att_sql_query)
    att_df = att_amrld

    att_df.to_csv(pid_list_save_to_file_path, index=False)
    print 'AT&T pidlist saved at : {}'.format(pid_list_save_to_file_path)

def build_att_pidlist_main():
    rs_client, s3_client_object = targets.initialize_redshift(config.redshift_creds, config.s3_creds, config.db_name)
    build_att_pidlist(rs_client, config.pid_list_save_to_file_path)



if __name__ == "__main__":
    if config.target_name == 'jaguar_proxy':
        build_jaguar_pidlist_main()
    elif config.target_name == 'metro_pcs_updated':
        build_metro_pcs_pidlist_main()
    elif config.target_name == 'legacy_toyota_prius':
        build_toyota_prius_pidlist_main()
    elif config.target_name == 'vtech':
        build_vtech_pidlist_main()
    elif config.target_name == 'att':
        build_att_pidlist_main()