"""
#!/usr/bin/env python2

Code to add target assignments. Written in python2

"""

import config
from common import redshift_client
from common import s3_client
import pandas as pd
import os
import sys


def initialize_redshift(redshift_creds, s3_creds, db_name):
    database = redshift_creds['database']
    host = redshift_creds['host']
    username = redshift_creds['user']
    password = redshift_creds['password']
    port = redshift_creds['port']

    os.environ['AWS_ACCESS_KEY_ID'] = s3_creds['access_key_id']
    os.environ['AWS_SECRET_ACCESS_KEY'] = s3_creds['secret_access_key']

    s3_client_object = s3_client.S3Client(bucket=s3_creds['bucket'])

    # s3_client_object = s3_client.S3Client(bucket=s3_creds['bucket'],
    #         access_key_id=s3_creds['access_key_id'],
    #         secret_access_key=s3_creds['secret_access_key'] ,)

    rs_client = redshift_client.RedshiftClient.from_credentials(
            host,
            port,
            database,
            username,
            password,
            s3_client_object)

    rs_client.execute('set search_path to {}'.format(db_name))
    print 'rs_client created'
    return rs_client, s3_client_object


def create_pid_list_table_on_redshift(rs_client, pid_list_table_name):
    if pid_list_table_name == None:
        sys.exit('Enter a pid_list_table_name')
    create_temp_table_query = """
       DROP TABLE IF EXISTS {0};
       CREATE TEMP TABLE {0}
        (hh_num BIGINT,
        person_id INT,
        start_date date,
        end_date date);
    
        """.format(pid_list_table_name)
    rs_client.execute(create_temp_table_query)
    print 'table_created'


def put_pidlist_on_redshift(rs_client, pid_list_file_path, pid_list_table_name):
    pids_from_pidlistbuilder = pd.read_csv(pid_list_file_path)
    #pids_from_pidlistbuilder['start_date'] = pd.to_datetime(pids_from_pidlistbuilder['start_date'])
    #pids_from_pidlistbuilder['end_date'] = pd.to_datetime(pids_from_pidlistbuilder['end_date'])
    print('-----------------')
    print(pids_from_pidlistbuilder.tail())
    rs_client.insert_dataframe_into_redshift(pids_from_pidlistbuilder, pid_list_table_name)

    print 'pidlist uploaded to redshift'


def add_target_id_into_db(rs_client, target_name, target_description, target_id_table):
    target_table_name = target_id_table

    max_id_number_query = """
    SELECT MAX(id)
    FROM {};
    """.format(target_table_name)
    max_id_number = rs_client.select_as_dataframe(max_id_number_query)
    max_id_number = max_id_number.values[0][0]
    target_id = max_id_number + 1

    sql_query = """
    INSERT INTO {}
    SELECT {} AS id,
           '{}' AS target,
           0 AS size,
           '{}' as pretty_description;
    """.format(target_table_name, target_id, target_name, target_description)
    rs_client.execute(sql_query)
    print '{} inserted into table {} as ID {}'.format(target_name, target_table_name, target_id)

    return target_id


def create_demo_sql_query(demo_info):
    age_range = demo_info[1:].split('-')
    if len(age_range) == 2:
        age_range_statement = 'AND amrld.age BETWEEN {} AND {}'.format(age_range[0], age_range[1])
    elif len(age_range) == 1:
            age_range_statement ='AND amrld.age >= {}'.format(age_range[0][:-1])

    gender = demo_info[0]
    if gender == 'M':
        gender_statement = "AND amrld.sex = 'M'"
    elif gender == 'F':
        gender_statement = "AND amrld.sex = 'F'"
    else:
        gender_statement = ' '
    demo_string = age_range_statement + ' ' + gender_statement
    print 'demo sql string created: {}'.format(demo_string)
    return demo_string


def create_full_pidlist_with_targets_assigned(rs_client,
                                              demo_info,
                                              target_name,
                                              target_id,
                                              pid_list_table_name,
                                              target_assignment_table,
                                              amrld_only=False):
    target_assignment_table_name = target_assignment_table
    print 'adding to {}'.format(target_assignment_table_name)
    print 'pulling from {}'.format(pid_list_table_name)

    sql_query_for_survey_start_date = """
        SELECT max(end_date)
        FROM {}
        """.format(pid_list_table_name)
    survey_cutoff_date = rs_client.select(sql_query_for_survey_start_date)
    survey_cutoff_date = survey_cutoff_date[0][0]

    demo_string = create_demo_sql_query(demo_info)

    if amrld_only:
        if_not_amrld_only_set_false = """"""
        print('amrld only target')
    else:
        if_not_amrld_only_set_false = """
                UPDATE tmp_target_pids_info
                SET is_target = FALSE
                WHERE db_entry_date < target_entry_date;
        """

    sql_query = """
    -- pidlist builder only, including demo
        DROP TABLE IF EXISTS tmp_target_pids_info;
        CREATE TEMP TABLE tmp_target_pids_info AS (
        SELECT -- select distinct ?
                  pid_list.hh_num,
                  pid_list.person_id,
                  pid_list.target_entry_date,
                  amrld.db_entry_date
                FROM
                  (SELECT
                     hh_num,
                     person_id,
                     start_date,
                     end_date,
                     MIN(start_date) OVER (PARTITION BY hh_num, person_id) AS target_entry_date
                   FROM {}) pid_list
                   left JOIN (
                     SELECT
                       hh_num,
                       person_id,
                       start_date,
                       end_date,
                       MIN(start_date) OVER (PARTITION BY hh_num, person_id) AS db_entry_date,
                              age,
                              sex
                      FROM amrld.amrld_npx_persons) amrld
                   USING (hh_num, person_id)
                   WHERE amrld.start_date = amrld.db_entry_date
                   {});
        ALTER TABLE tmp_target_pids_info
          ADD COLUMN is_target BOOLEAN
          DEFAULT TRUE;
        {}
        ALTER TABLE tmp_target_pids_info
          ADD COLUMN pid INTEGER
          DEFAULT 0;
        UPDATE tmp_target_pids_info
          SET pid = hh_num*100 + person_id;
        ALTER TABLE tmp_target_pids_info
            DROP COLUMN db_entry_date;
        ALTER TABLE tmp_target_pids_info
            DROP COLUMN target_entry_date;

        --including all neilsen panelists
        DROP TABLE IF EXISTS tmp_all_pids_info;
        CREATE TEMP TABLE tmp_all_pids_info AS(
          SELECT
          *
          FROM (
          SELECT
            *,
            hh_num*100 + person_id pid
          FROM (
            SELECT
              hh_num,
              person_id,
              t.db_entry_date
            FROM
              (SELECT
                hh_num,
                person_id,
                start_date,
                end_date,
                MIN(start_date)OVER (PARTITION BY hh_num, person_id) AS db_entry_date
               FROM amrld.amrld_npx_persons) t
          WHERE t.start_date = t.db_entry_date) t1)
          LEFT JOIN tmp_target_pids_info
          USING (hh_num, person_id, pid)
        );
        UPDATE tmp_all_pids_info
        SET is_target = FALSE
        WHERE db_entry_date <= '{}'
        AND is_target IS NULL
        ;
        ALTER TABLE tmp_all_pids_info
          DROP COLUMN db_entry_date;
        ALTER TABLE tmp_all_pids_info
          ADD COLUMN target_name VARCHAR(256)
          DEFAULT '{}';
        ALTER TABLE tmp_all_pids_info
          ADD COLUMN target INT
          DEFAULT {};
        
        -- Removing duplicates coming from pid list from MRIpidbuilder
        INSERT INTO {} (hh_num, person_id, pid, target, target_name, is_target)
          SELECT hh_num, person_id, pid, target, target_name, is_target FROM 
            (SELECT DISTINCT * FROM tmp_all_pids_info)
          ;
    """.format(pid_list_table_name, demo_string, if_not_amrld_only_set_false,
               survey_cutoff_date, target_name, target_id, target_assignment_table_name)

    rs_client.execute(sql_query)
    print 'new target {} added to {}'.format(target_name, target_assignment_table_name)

def pull_survey_start_date(rs_client, pid_list_table_name):
    sql_query_for_survey_start_date = """
        SELECT max(start_date)
        FROM {}
        """.format(pid_list_table_name)
    survey_start_date = rs_client.select(sql_query_for_survey_start_date)
    survey_start_date = survey_start_date[0][0]
    return survey_start_date

def pull_survey_end_date(rs_client, pid_list_table_name):
    sql_query_for_survey_end_date = """
         SELECT min(end_date)
         FROM {}
         """.format(pid_list_table_name)
    survey_end_date = rs_client.select(sql_query_for_survey_end_date)
    survey_end_date = survey_end_date[0][0]
    return survey_end_date

def pull_target_id(rs_client, target_id_table, target_name):
    sql_query_for_target_id = """
    SELECT ID
    FROM {}
    WHERE target = '{}';
    """.format(target_id_table, target_name)
    target_id = rs_client.select(sql_query_for_target_id)
    target_id = target_id[0][0]
    return target_id

def update_target_assignment_table(rs_client,
                                   target_assignment_table,
                                   pid_list_table_name,
                                   target_id_table,
                                   target_name,
                                   demo_info):
    target_assignment_table_name = target_assignment_table
    print 'adding to {}'.format(target_assignment_table_name)
    print 'pulling from {}'.format(pid_list_table_name)

    survey_start_date = pull_survey_start_date(rs_client, pid_list_table_name)
    survey_end_date = pull_survey_end_date(rs_client, pid_list_table_name)
    target_id = pull_target_id(rs_client, target_id_table, target_name)
    demo_string = create_demo_sql_query(demo_info)

    sql_query = """
        -- pidlist builder table only, adding demo info (if included)
        DROP TABLE IF EXISTS tmp_update_pids;
        CREATE TEMP TABLE tmp_update_pids AS (
        SELECT
                  pid_list.hh_num,
                  pid_list.person_id,
                  pid_list.target_entry_date,
                  amrld.db_entry_date
                FROM
                  (SELECT
                     hh_num,
                     person_id,
                     start_date,
                     end_date,
                     MIN(start_date) OVER (PARTITION BY hh_num, person_id) AS target_entry_date
                   FROM {}) pid_list --pidlisttablename
                   LEFT JOIN (
                     SELECT
                       hh_num,
                       person_id,
                       start_date,
                       end_date,
                       MIN(start_date)
                     OVER (PARTITION BY hh_num, person_id) AS db_entry_date,
                              age,
                              sex
                      FROM amrld.amrld_npx_persons) amrld
                   USING (hh_num, person_id)
                   WHERE amrld.start_date = amrld.db_entry_date
                   {}); --demo info
        ALTER TABLE tmp_update_pids
          ADD COLUMN pid INTEGER
          DEFAULT 0;
        UPDATE tmp_update_pids
          SET pid = hh_num*100 + person_id;
        ALTER TABLE tmp_update_pids
          ADD COLUMN is_target BOOLEAN
          DEFAULT NULL;
        UPDATE tmp_update_pids
          SET is_target = TRUE
          WHERE db_entry_date >= target_entry_date
          AND is_target IS NULL;
        ALTER TABLE tmp_update_pids
            DROP COLUMN target_entry_date;

        --pulling db_entry date of panelists with null in vantage table
        DROP TABLE IF EXISTS tmp_all_pids_info;
        CREATE TEMP TABLE tmp_all_pids_info AS (
          SELECT * FROM
                        (SELECT *
          FROM {} -- fox.vantage_target_assignments_mk
        WHERE target_name = '{}' -- target being updated
        AND is_target IS NULL )
        LEFT JOIN (SELECT
            *,
            hh_num*100 + person_id pid
          FROM (
            SELECT
              hh_num,
              person_id,
              t.db_entry_date
            FROM
              (SELECT
                hh_num,
                person_id,
                start_date,
                end_date,
                MIN(start_date)OVER (PARTITION BY hh_num, person_id) AS db_entry_date
               FROM amrld.amrld_npx_persons) t
          WHERE t.start_date = t.db_entry_date) )
          USING (hh_num, person_id, pid) );

        -- update null values
        DROP TABLE IF EXISTS tmp_updated_and_new_pids;
        CREATE TEMP TABLE tmp_updated_and_new_pids AS (
        SELECT DISTINCT * FROM (
        SELECT hh_num, person_id, pid, db_entry_date, COALESCE(tmp_update_pids.is_target, tmp_all_pids_info.is_target) is_target
        FROM tmp_all_pids_info
        FULL OUTER JOIN tmp_update_pids
        USING (hh_num, person_id, pid, db_entry_date)
          ));
        UPDATE tmp_updated_and_new_pids
        SET is_target = FALSE
        WHERE db_entry_date >= '{}' --survey start date
          AND db_entry_date <= '{}' -- survey end date
        AND is_target IS NULL;
        ALTER TABLE tmp_updated_and_new_pids
            DROP COLUMN db_entry_date;
        ALTER TABLE tmp_updated_and_new_pids
            ADD COLUMN target INT
            DEFAULT {}; --target id number
        ALTER TABLE tmp_updated_and_new_pids
            ADD COLUMN target_name VARCHAR(256)
            DEFAULT '{}'; --target_name

    -- update entries in vantage table      
    UPDATE {} --vantage table
    SET is_target = rt.is_target
    FROM tmp_updated_and_new_pids rt
    WHERE {}.hh_num = rt.hh_num --vantage table
    AND {}.person_id = rt.person_id --vantage table
    AND {}.target_name = rt.target_name --vantage table
    AND {}.target = rt.target --vantage table
    AND {}.is_target IS NULL; --vantage table

    -- add new neilsen panelists into vantage table
   INSERT INTO {} (hh_num, person_id, pid, is_target, target, target_name) --vantage table
   SELECT hh_num, person_id, pid, is_target, target, target_name FROM tmp_updated_and_new_pids t
   WHERE t.pid NOT IN (
                     SELECT DISTINCT pid
                     FROM {} -- vantage table
                     WHERE target_name = '{}') -- target name
                     ;
    
    """.format(pid_list_table_name, demo_string, target_assignment_table_name,
               target_name, survey_start_date, survey_end_date, target_id, target_name,
               target_assignment_table_name, target_assignment_table_name,
               target_assignment_table_name, target_assignment_table_name,
               target_assignment_table_name, target_assignment_table_name,
               target_assignment_table_name,
               target_assignment_table_name, target_name)

    rs_client.execute(sql_query)
    print 'target {} updated in {}'.format(target_name, target_assignment_table_name)
    print 'table updated'


if __name__ == "__main__":
    rs_client, s3_client_object = initialize_redshift(config.redshift_creds,
                                                      config.s3_creds, config.db_name)

    # s3_client = S3Client(bucket=s3_bucket)
    # temp_s3_client = s3_client  # temp bucket (delete perms ok?)
    # redshift_client = RedshiftClient.from_credentials(s3_client=temp_s3_client,
    #                                                   host=host, port=port, database=database,
    #                                                   username=user, password=password)

    sql_query = """
    SELECT id FROM {} WHERE target='{}';
    """.format(config.target_id_table, config.target_name)
    target_table_status = rs_client.select(sql_query)
    target_table_status = 'new' if target_table_status == [] else 'update'
    print 'target {} is {}'.format(config.target_name, target_table_status)

    if (config.pid_list_table_name == None) and (config.pid_list_file_path != None):
        pid_list_table_name = 'temp_pid_list'
        create_pid_list_table_on_redshift(rs_client, pid_list_table_name)
        put_pidlist_on_redshift(rs_client, config.pid_list_file_path, pid_list_table_name)
    elif (config.pid_list_table_name != None) and (config.pid_list_file_path == None):
        pid_list_table_name = config.pid_list_table_name
    else:
        print 'check pid list path in config file: Set one option to "None"'
        exit(1)

    if target_table_status == 'new':
        target_id = add_target_id_into_db(rs_client, config.target_name, config.target_description,
                                          config.target_id_table)
        create_full_pidlist_with_targets_assigned(rs_client, config.demo, config.target_name, target_id,
                                                  pid_list_table_name, config.target_assignment_table,
                                                  config.amrld_only)
    elif target_table_status == 'update':
        update_target_assignment_table(rs_client, config.target_assignment_table, pid_list_table_name,
                                       config.target_id_table, config.target_name, config.demo)
    else:
        print 'there is an error in the target_table_status check'
        exit(1)

    print 'completed'


