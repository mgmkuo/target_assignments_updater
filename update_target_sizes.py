"""
#!/usr/bin/env python2

Updating target sizes
Requires config file for AWS keys and table names
"""

import config
import assign_and_upload_targets as targets
import datetime


def update_target_size(rs_client, target_assignment_table, table_id_table):
    pull_end_date_sql_query = """
    SELECT MAX(sunday) FROM amrld.amrld_npx_person_weights;
    """
    end_date = rs_client.select(pull_end_date_sql_query)
    end_date = end_date[0][0] - datetime.timedelta(weeks=12)
    start_date = end_date - datetime.timedelta(weeks=12)
    print 'Averaging values from {} to {}'.format(start_date, end_date)

    pull_end_br_date_sql_query = """
    SELECT DISTINCT br_dt_key FROM amrld.amrld_npx_person_weights
    WHERE sunday = '{}'; -- start/end date
    """.format(end_date)
    end_br_date = rs_client.select(pull_end_br_date_sql_query)
    end_br_date = end_br_date[0][0]

    pull_start_br_date_sql_query = """
    SELECT DISTINCT br_dt_key FROM amrld.amrld_npx_person_weights
    WHERE sunday = '{}'; -- start/end date
    """.format(start_date)
    start_br_date = rs_client.select(pull_start_br_date_sql_query)
    start_br_date = start_br_date[0][0]

    pull_all_target_names_sql_query = """
    SELECT target FROM {};
    """.format(table_id_table)
    targets_list = rs_client.select_as_dataframe(pull_all_target_names_sql_query)
    for target_name in targets_list.values:
        target_name = target_name[0]
        calculate_size_sql_query = """
            SELECT AVG(size)
            FROM (
            SELECT
               SUM(weekly_unified_weight) AS size
            FROM(
              SELECT target, target_name,
                     w.br_dt_key,
                     (w.hh_num * 100 + w.person_id) AS pid,
                     (w.weekly_unified_weight) AS weekly_unified_weight
              FROM amrld.amrld_npx_person_weights w
              INNER JOIN {} v --fox.vantage_target_assignments_mk
                     ON (w.hh_num * 100 + w.person_id) = v.pid
              WHERE v.is_target IS TRUE
              AND v.target_name = '{}'
            )
            WHERE br_dt_key BETWEEN {} AND {} --change dates
            GROUP BY br_dt_key, target, target_name);
            """.format(target_assignment_table, target_name, start_br_date, end_br_date)
        target_size = rs_client.select(calculate_size_sql_query)
        target_size = target_size[0][0]

        update_target_id_table_sql_query = """
        UPDATE {}
        SET size = {}
        WHERE target = '{}';
        """.format(table_id_table, target_size, target_name)
        rs_client.execute(update_target_id_table_sql_query)

        print 'Target size for {} updated to {}'.format(target_name, target_size)
    print 'Target size update done'

if __name__ == "__main__":
    rs_client, s3_client_object = targets.initialize_redshift(config.redshift_creds, config.s3_creds, config.db_name)
    update_target_size(rs_client, config.target_assignment_table, config.target_id_table)